/**
 * @file karbon-game.cpp
 * @date 16-Jun-2022
 */

#define __SCROLL_IMPL__
#include "karbon-game.h"
#undef __SCROLL_IMPL__

#include "Ship.h"
#include "Shield.h"
#include "Enemy.h"
#include "Laser.h"
#include "ProgressBar.h"

Ship *ship;
ProgressBar *progressBar;
ScrollObject *titleCard;
//#include "Laser.h"

#ifdef __orxMSVC__

/* Requesting high performance dedicated GPU on hybrid laptops */
__declspec(dllexport) unsigned long NvOptimusEnablement        = 1;
__declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;

#endif // __orxMSVC__

void karbon_game::RemoveAllFloatingShields(){
		//orxSPAWNER *spawner = orxSPAWNER(orxStructure_GetNext(shieldMaker));

	orxSPAWNER *spawner = orxOBJECT_GET_STRUCTURE(shieldMaker, SPAWNER);



	orxOBJECT *pstChild = orxOBJECT(orxStructure_GetChild(spawner));
		//orxOBJECT *pstChild2 = orxObject_GetOwnedSibling(pstChild);

	for (orxOBJECT *pstObject = shieldMaker; 
     pstObject != orxNULL; 
     pstObject = orxOBJECT(orxStructure_GetNext(pstObject))){
		 int here = 1;
	 }
	
		for (orxOBJECT *pstChild = orxObject_GetOwnedChild(shieldMaker);
			pstChild;
			pstChild = orxObject_GetOwnedSibling(pstChild))
			{
				orxObject_SetLifeTime(pstChild, 0);
//				const orxSTRING name = orxObject_GetName(pstChild);
//				if (orxString_Compare(name, childName) == 0) {
//					return pstChild;
//				}
			}
	 
}

MODES karbon_game::GetGameMode(){
	return gameMode;
}

void karbon_game::SetGameMode(MODES mode){
	if (mode != gameMode){
		previousGameMode = gameMode;
		gameMode = mode;
	}
}

/** Update function, it has been registered to be called every tick of the core clock
 */
void karbon_game::Update(const orxCLOCK_INFO &_rstInfo)
{
    // Should quit?
    if(orxInput_IsActive("Quit"))
    {
        // Send close event
        orxEvent_SendShort(orxEVENT_TYPE_SYSTEM, orxSYSTEM_EVENT_CLOSE);
    }
	
	if (gameMode == GAME_MODE_GATHER && previousGameMode != GAME_MODE_GATHER){// (orxInput_HasBeenActivated("GatherMode")){
		if (enemyMaker != orxNULL){
			orxObject_SetLifeTime(enemyMaker, 0);
			enemyMaker = orxNULL;
		}
		if (shieldMaker == orxNULL){
			shieldMaker = orxObject_CreateFromConfig("ShieldMaker");
		}
		//orxObject_Enable(shieldMaker, orxTRUE);
		progressBar->CalmMode();
	}
	
	if (gameMode == GAME_MODE_DEFEND && previousGameMode != GAME_MODE_DEFEND){ //if (orxInput_HasBeenActivated("AttackMode")){
		if (enemyMaker == orxNULL){
			//orxObject_Enable(shieldMaker, orxFALSE);
			//RemoveAllFloatingShields();
			
			enemyMaker = orxObject_CreateFromConfig("EnemyMaker");
		}
		if (shieldMaker != orxNULL){
			orxObject_SetLifeTime(shieldMaker, 0);
			shieldMaker = orxNULL;
		}
		progressBar->PanicMode();
	}
	
	if (gameMode == GAME_MODE_TITLE && previousGameMode != GAME_MODE_TITLE){ //if (orxInput_HasBeenActivated("AttackMode")){
		progressBar->Pause(orxTRUE);
		
		if (enemyMaker != orxNULL){
			//orxObject_Enable(shieldMaker, orxFALSE);
			orxObject_SetLifeTime(enemyMaker, 0);
			enemyMaker = orxNULL;
			//RemoveAllFloatingShields();
		}
		if (shieldMaker != orxNULL){
			orxObject_SetLifeTime(shieldMaker, 0);
			shieldMaker = orxNULL;
		}
		
		ship->Enable(orxFALSE);
		progressBar->Enable(orxFALSE);
		titleCard->Enable(orxTRUE);
	}
	
	if (progressBar->GetProgressPercent() == 0){
		progressBar->SetProgressPercent(100);
		if (gameMode == GAME_MODE_DEFEND){
			gameMode = GAME_MODE_GATHER;
			progressBar->CalmMode();
		} else {
			gameMode = GAME_MODE_DEFEND;
			progressBar->PanicMode();
		}
		
	}
	
	if(orxInput_HasBeenActivated("Fire"))
    {
		if (gameMode == GAME_MODE_TITLE){
			SetGameMode(GAME_MODE_GATHER);
			progressBar->Enable(orxTRUE);
			ship->Enable(orxTRUE);
			titleCard->Enable(orxFALSE);
			progressBar->Pause(orxFALSE);
		}
    }

}

/** Init function, it is called when all orx's modules have been initialized
 */
orxSTATUS karbon_game::Init()
{
	//orxViewport_CreateFromConfig("WrapRightViewport");
	//orxViewport_CreateFromConfig("WrapLeftViewport");

    // Display a small hint in console
    orxLOG("\n* This template project creates a simple scene"
    "\n* You can play with the config parameters in ../data/config/karbon-game.ini"
    "\n* After changing them, relaunch the executable to see the changes.");

    // Create the scene
    ship = CreateObject<Ship>("Ship");
	ship->Enable(orxFALSE);
	//CreateObject("Enemy");

	orxObject_CreateFromConfig("GameScene");

	progressBar = (ProgressBar *)CreateObject("ProgressBar");
    //orxFLOAT percent = progressBar->GetProgressPercent();
    progressBar->SetProgressPercent(100);
    progressBar->Pause(orxTRUE);
    progressBar->CalmMode();
	progressBar->Enable(orxFALSE);
    //progressBar->CalmMode();

	gameMode = GAME_MODE_TITLE;

	enemyMaker = orxNULL;
	shieldMaker = orxNULL;
	//shieldMaker = orxObject_CreateFromConfig("ShieldMaker");
	//orxObject_Enable(shieldMaker, orxFALSE);

	titleCard = CreateObject("TitleCard");
	//titleCard->Enable(orxFALSE);

    // Done!
    return orxSTATUS_SUCCESS;
}

/** Run function, it should not contain any game logic
 */
orxSTATUS karbon_game::Run()
{
    // Return orxSTATUS_FAILURE to instruct orx to quit
    return orxSTATUS_SUCCESS;
}

/** Exit function, it is called before exiting from orx
 */
void karbon_game::Exit()
{
    // Let orx clean all our mess automatically. :)
}

/** BindObjects function, ScrollObject-derived classes are bound to config sections here
 */
void karbon_game::BindObjects()
{
    // Bind the Object class to the Object config section
    ScrollBindObject<Ship>("Ship");
	ScrollBindObject<Shield>("Shield");
	ScrollBindObject<Enemy>("Enemy");
	ScrollBindObject<Laser>("Laser");
	ScrollBindObject<ProgressBar>("ProgressBar");
}

orxVECTOR karbon_game::GetPlayerPosition(){
	orxVECTOR p = {};
	ship->GetPosition(p, orxFALSE);
	
	return p;
}

/** Bootstrap function, it is called before config is initialized, allowing for early resource storage definitions
 */
orxSTATUS karbon_game::Bootstrap() const
{
    // Add config storage to find the initial config file
    orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../data/config", orxFALSE);

    // Return orxSTATUS_FAILURE to prevent orx from loading the default config file
    return orxSTATUS_SUCCESS;
}

/** Main function
 */
int main(int argc, char **argv)
{
    // Execute our game
    karbon_game::GetInstance().Execute(argc, argv);

    // Done!
    return EXIT_SUCCESS;
}
