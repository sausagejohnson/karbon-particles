/*
 Object makeup is:

 ProgressBar
    ProgressBarBackground
    ProgressBarLineLeft
    ProgressBarLineRight
    ProgressBarLineTop
    ProgressBarLineBottom
    ProgressBarLevel
*/

#include "ProgressBar.h"

void ProgressBar::OnCreate()
{
    orxConfig_SetBool("IsObject", orxTRUE);
    progressPercent = 0;
    isPaused = orxTRUE;
}

void ProgressBar::OnDelete()
{
}

void ProgressBar::Update(const orxCLOCK_INFO &_rstInfo)
{
	MODES mode = karbon_game::GetInstance().GetGameMode();
	orxFLOAT stepSize = 0.2;
	if (mode == GAME_MODE_DEFEND){
		stepSize = 0.3;
	} 
	
    if (!isPaused){
		if (progressPercent > 0){
			progressPercent -= stepSize;
		}
        if (progressPercent < 0){
            progressPercent = 0;
        }
        
        ScrollObject* level = GetChildByName("ProgressBarLevel");
    
        orxFLOAT fractionOfProgress = progressPercent/100;
        
        orxVECTOR sv = {fractionOfProgress, 1};
        level->SetScale(sv, orxFALSE);
    }
}

void ProgressBar::Pause(orxBOOL onOff){
    isPaused = onOff;
}


orxFLOAT ProgressBar::GetProgressPercent(){
    ScrollObject* level = GetChildByName("ProgressBarLevel");
    
    orxVECTOR sv = {};
    level->GetScale(sv, orxFALSE);
    
    return sv.fX * 100;
}


void ProgressBar::SetProgressPercent(orxFLOAT percent){
    progressPercent = percent;
}

void ProgressBar::PanicMode(){
    orxVECTOR darkRed = {0.3, 0, 0.1}; //98, 0, 41    A:50
    orxVECTOR brightRed = {1, 0, 0.6}; //255, 0, 153
    
    ColourTheme(darkRed, brightRed);
}
void ProgressBar::CalmMode(){
    orxVECTOR darkBlue = {0, 0.99, 0.76}; //1, 253, 196   A:50
    orxVECTOR brightBlue = {0, 0.37, 0.42}; //4, 96, 109		
    
    ColourTheme(darkBlue, brightBlue);
}

void ProgressBar::ColourTheme(orxVECTOR backgroundColour, orxVECTOR levelColour){
    ScrollObject* background = GetChildByName("ProgressBarBackground");
    ScrollObject* level = GetChildByName("ProgressBarLevel");
       
    orxCOLOR colour;
    colour.vRGB = backgroundColour;
    colour.fAlpha = 0.5;
    
    background->SetColor(colour);
    
    colour.vRGB = levelColour;
    colour.fAlpha = orxFLOAT_1;
    
    level->SetColor(colour);
}

ScrollObject* ProgressBar::GetChildByName(const orxSTRING childName) {
 
	for (ScrollObject *child = this->GetOwnedChild(); child; child = child->GetOwnedSibling()) {
		if (orxString_Compare(child->GetModelName(), childName) == 0) {
			//child->SetLifeTime(0);
			return child;
		}
	}
    
    return orxNULL;
}
