#ifndef ENEMY_HPP
#define ENEMY_HPP

#include "karbon-game.h"

/** Object Class
 */
class Enemy : public ScrollObject
{
public:


protected:

                void            OnCreate();
                void            OnDelete();
                void            Update(const orxCLOCK_INFO &_rstInfo);
				orxBOOL 		OnCollide(ScrollObject *_poCollider,
									orxBODY_PART *_pstPart,
									orxBODY_PART *_pstColliderPart,
									const orxVECTOR &_rvPosition,
									const orxVECTOR &_rvNormal);

private:
				
};

#endif // ENEMY_HPP
