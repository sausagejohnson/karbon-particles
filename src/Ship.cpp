/**
 * @file Object.cpp
 * @date 16-Jun-2022
 */

#include "Ship.h"

void Ship::OnCreate()
{
    orxConfig_SetBool("IsObject", orxTRUE);
	
	trigger = GetChildByName("GunTrigger");
	trigger->Enable(orxFALSE, orxFALSE); //stop firing on start
}

void Ship::OnDelete()
{
}

void Ship::ClearShieldBodyParts(){
	orxOBJECT *shipObject = this->GetOrxObject();
	orxBODY *shipBody = orxOBJECT_GET_STRUCTURE(shipObject, BODY);

	orxBODY_PART *part = orxBody_GetNextPart(shipBody, orxNULL);
	while (part != orxNULL){
		orxBODY_PART *nextPart = orxBody_GetNextPart(shipBody, part);
		
		if (orxString_Compare(orxBody_GetPartName(part), "ShieldBodyPart") == 0) {
			orxBody_RemovePart(part);
		}			
		part = nextPart;
	}
}

int Ship::CountShieldBodyParts(){
	int count = 0;
	
	orxOBJECT *shipObject = this->GetOrxObject();
	orxBODY *shipBody = orxOBJECT_GET_STRUCTURE(shipObject, BODY);

	orxBODY_PART *part = orxBody_GetNextPart(shipBody, orxNULL);
	while (part != orxNULL){
		orxBODY_PART *nextPart = orxBody_GetNextPart(shipBody, part);
		
		if (orxString_Compare(orxBody_GetPartName(part), "ShieldBodyPart") == 0) {
			count++;
		}			
		part = nextPart;
	}
	
	return count;
}

void Ship::Update(const orxCLOCK_INFO &_rstInfo)
{
	if (orxInput_IsActive("Test"))
	{

		int here = 1;
    }
	
	if (orxInput_IsActive("Thrust"))
    {
		//orxLOG("Thrust");
		this->Thrust();
    }
	if (orxInput_IsActive("RotateLeft"))
    {
		//orxLOG("RotateLeft");
		this->RotateLeft();
    }
	if(orxInput_IsActive("RotateRight"))
    {
		//orxLOG("RotateRight");
		this->RotateRight();
    }
	
	if (orxInput_HasBeenActivated("Fire"))
    {
		if (karbon_game::GetInstance().GetGameMode() == GAME_MODE_GATHER){
			trigger->Enable(orxTRUE, orxFALSE); //stop firing
		}
    }
	
	if (orxInput_HasBeenDeactivated("Fire"))
    {
		//if (karbon_game::GetInstance().GetGameMode() == GAME_MODE_GATHER){
			trigger->Enable(orxFALSE, orxFALSE); //stop firing
		//}
    }
	

	
	orxFLOAT maxX = 375; //400;
	orxFLOAT maxY = 225; //250;
	
	orxVECTOR v = {};
	this->GetPosition(v, orxFALSE);
	
	if (v.fX > 400){
		v.fX = -345;
		this->SetPosition(v, orxTRUE);
	}

	if (v.fX < -400){
		v.fX = 345;
		this->SetPosition(v, orxTRUE);
	}

	if (v.fY > maxY){
		v.fY = -maxY;
		this->SetPosition(v, orxTRUE);
	}

	if (v.fY < -maxY){
		v.fY = maxY;
		this->SetPosition(v, orxTRUE);
	}
	
	

}

//void Ship::PostDebugSpot(orxVECTOR position) {
//	orxObject_SetPosition(debugSpot, &position);
//}

void Ship::RotateLeft(){
	orxFLOAT rotation = this->GetRotation(orxFALSE);
	rotation -= 0.1;
	this->SetRotation(rotation, orxFALSE);
//	orxOBJECT *shipObject = this->GetOrxObject();
//	orxObject_SetAngularVelocity(shipObject, -0.5);
	
}
void Ship::RotateRight(){
	orxFLOAT rotation = this->GetRotation(orxFALSE);
	rotation += 0.1;
	this->SetRotation(rotation, orxFALSE);
}

void Ship::Thrust(){
	
	orxOBJECT *shipObject = this->GetOrxObject();
	//orxBODY *body = orxOBJECT_GET_STRUCTURE(shipObject, BODY);
	
	orxVECTOR speedVector = {};
	orxObject_GetRelativeSpeed(shipObject, &speedVector);
	//orxLOG("speed for capping: %f %f", v.fX, v.fY);
	orxFLOAT size = orxVector_GetSize(&speedVector);
	//orxLOG("vec size: %f", size);
//	if (size > 400) {
//		return; //cap the thrust
//	}
	
	orxVECTOR localInfluencePoint = { 0, 25, 0 };
	
	orxFLOAT FORCE = 8.0;
	orxVECTOR thrust = { 0, -FORCE, 0 };
 
	orxVECTOR thrustDirection = { 0,0,0 };
	orxVECTOR thrustPosition = orxVECTOR_0;
 
	orxVECTOR objectPosition = orxVECTOR_0;
	orxObject_GetPosition(shipObject, &objectPosition);
 
	orxFLOAT objectRadians = orxObject_GetRotation(shipObject);
	orxFLOAT facingRotation = (orxMATH_KF_RAD_TO_DEG * objectRadians) + 0;
 
	orxVector_2DRotate(&thrustDirection, &thrust, objectRadians);
 
	orxVector_2DRotate(&localInfluencePoint, &localInfluencePoint, objectRadians);
 
	orxVector_Add(&thrustPosition, &objectPosition, &localInfluencePoint);
 
//	orxObject_ApplyForce(shipObject, &thrustDirection, &thrustPosition);
	orxObject_ApplyImpulse(shipObject, &thrustDirection, &thrustPosition);
	
	int here = 1;
	//PostDebugSpot(thrustPosition);
}

orxBOOL Ship::OnCollide(ScrollObject *_poCollider,
	orxBODY_PART *_pstPart,
	orxBODY_PART *_pstColliderPart,
	const orxVECTOR &_rvPosition,
	const orxVECTOR &_rvNormal)
{
	if (_poCollider == orxNULL) {
		return orxTRUE;
	}

	const orxSTRING colliderName = _poCollider->GetModelName();
	if (orxString_SearchString(colliderName, "Shield") != orxNULL) {
		orxOBJECT *shipObject = this->GetOrxObject();
		orxOBJECT *shieldObject = _poCollider->GetOrxObject();
		ScrollObject *shield = _poCollider;
		
		orxVECTOR localShieldPosition = {};
		orxVECTOR shipPosition = {};
		orxVECTOR shieldPosition = {};
		this->GetPosition(shipPosition, orxTRUE);
		shield->GetPosition(shieldPosition, orxTRUE);
		orxVector_Sub(&localShieldPosition, &shieldPosition, &shipPosition);
		
		
		orxBODY *shipBody = orxOBJECT_GET_STRUCTURE(shipObject, BODY);
		orxBODY *thisShieldBody = orxOBJECT_GET_STRUCTURE(shieldObject, BODY);

		orxBODY_PART *shieldPart = orxBody_GetNextPart(thisShieldBody, orxNULL);		
		orxSTATUS stat = orxBody_RemovePart(shieldPart);

		if (thisShieldBody){
			orxObject_UnlinkStructure(shieldObject, orxStructure_GetID(thisShieldBody));
			//does the above release the memory?
		}
		
		orxObject_SetParent(shieldObject, shipObject);
		orxObject_SetOwner(shieldObject, shipObject);
		


		_poCollider->SetPosition(_rvPosition, orxTRUE);
		//orxLOG("Added to ship parent");
		
		orxVECTOR v = {};
		orxObject_GetPosition(shieldObject, &v);
		
		//Alter shield part position to place over newly parented object
		orxConfig_PushSection("ShieldBodyPart");
		orxConfig_SetVector("Center", &v);// &localShieldPosition);
		orxConfig_SetU64("Radius", 30);// &localShieldPosition);
		orxConfig_SetU64("ObjectShieldID", shield->GetGUID());
		orxConfig_PopSection();
		
		orxLOG("shield picked up, %lld", shield->GetGUID());
		

		orxBODY_PART *part = orxBody_AddPartFromConfig(shipBody, "ShieldBodyPart");
		
		orxConfig_PushSection("PartTable");
		orxCHAR formattedPartPointer[32];
		orxString_NPrint(formattedPartPointer, sizeof(formattedPartPointer) - 1, "%016llX", part);
		orxLOG(formattedPartPointer);
		orxConfig_SetU64(formattedPartPointer, shield->GetGUID());
		orxConfig_PopSection();
		

		
		orxVECTOR resetPivot = {118, 146, 0}; //29, 44.5, 0};
		orxObject_SetPivot(shipObject, &resetPivot);

	}

	return orxTRUE;
}


ScrollObject* Ship::GetChildByName(const orxSTRING childName) {
 
	for (ScrollObject *child = this->GetOwnedChild(); child; child = child->GetOwnedSibling()) {
		if (orxString_Compare(child->GetModelName(), childName) == 0) {
			//child->SetLifeTime(0);
			return child;
		}
	}
    
    return orxNULL;
}