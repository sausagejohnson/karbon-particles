/**
 * @file Object.cpp
 * @date 16-Jun-2022
 */

#include "Shield.h"

void Shield::OnCreate()
{
    orxConfig_SetBool("IsObject", orxTRUE);
	trackToShip = orxFALSE;
}

void Shield::OnDelete()
{
}

void Shield::SetTrackToShip(orxBOOL onOff){
	trackToShip = onOff;
}

void Shield::Update(const orxCLOCK_INFO &_rstInfo)
{
	if (trackToShip){
		orxOBJECT *shieldObject = this->GetOrxObject();

		//Get this enemy position
		orxVECTOR shieldPosition = orxVECTOR_0;
		GetPosition(shieldPosition, orxTRUE);


		//Get real Position Vector of Block, used to popen fire when close
		orxVECTOR playerPosition = orxVECTOR_0;
		playerPosition = karbon_game::GetInstance().GetPlayerPosition();

		//Get real Direction to fire on outpost
		orxVECTOR directionToShip = orxVECTOR_0;
		orxVector_Sub(&directionToShip, &playerPosition, &shieldPosition);

		orxVECTOR speedTowardsShip = orxVECTOR_0;
		orxVector_Normalize(&speedTowardsShip, &directionToShip);
		orxVector_Mulf(&speedTowardsShip, &speedTowardsShip, 1.5);

		//Only allow a capped speed
		orxObject_ApplyImpulse(shieldObject, &speedTowardsShip, orxNULL);
	}
	
}


orxBOOL Shield::OnCollide(ScrollObject *_poCollider,
	orxBODY_PART *_pstPart,
	orxBODY_PART *_pstColliderPart,
	const orxVECTOR &_rvPosition,
	const orxVECTOR &_rvNormal)
{
	if (_poCollider == orxNULL) {
		return orxTRUE;
	}

	const orxSTRING colliderName = _poCollider->GetModelName();
	if (orxString_SearchString(colliderName, "Laser") != orxNULL) {
		trackToShip = orxTRUE;
		_poCollider->SetLifeTime(0);
		
		orxVECTOR shieldPosition = {};
		this->GetPosition(shieldPosition, orxFALSE);
		
		orxOBJECT *explosion = orxObject_CreateFromConfig("SmallExplosionMaker");
		orxObject_SetPosition(explosion, &shieldPosition);

	}
	
	if (orxString_SearchString(colliderName, "Ship") != orxNULL) {
		trackToShip = orxFALSE;
	}

	return orxTRUE;
}

