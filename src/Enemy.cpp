#include "Enemy.h"

void Enemy::OnCreate()
{
	orxConfig_SetBool("IsObject", orxTRUE);

	int directionChoice = orxMath_GetRandomFloat(0, 4);
	
	orxFLOAT maxX = 300;
	orxFLOAT maxY = 200;

    
	//orxVECTOR shipPosition = {};
	//shipPosition = karbon_game::GetInstance().GetPlayerPosition();
	
	orxVECTOR thisPosition = {};
	this->GetPosition(thisPosition, orxFALSE);
	orxLOG("%d", directionChoice);
	switch(directionChoice) {
	   case 0: //top
		  thisPosition = { orxMath_GetRandomFloat(-maxX, maxX), -maxY };
		  break;
	   case 1: //right
		  thisPosition = { maxX, orxMath_GetRandomFloat(-maxY, maxY) };
		  break; 
	   case 2: //bottom
		  thisPosition = { orxMath_GetRandomFloat(-maxX, maxX), maxY };
		  break; 
	   case 3: //left
		  thisPosition = { -maxX, orxMath_GetRandomFloat(-maxY, maxY) };
		  break; 	  
	}
	
	this->SetPosition(thisPosition, orxFALSE);
	
	//orxVector
}

void Enemy::OnDelete()
{
}

void Enemy::Update(const orxCLOCK_INFO &_rstInfo)
{
//	if (Outpost::GetInstance().IsGameEnabled() == orxFALSE) {
//		return;
//	}

	orxOBJECT *enemyObject = this->GetOrxObject();

	//Get this enemy position
	orxVECTOR enemyPosition = orxVECTOR_0;
	GetPosition(enemyPosition, orxTRUE);


	//Get real Position Vector of Block, used to popen fire when close
	orxVECTOR playerPosition = orxVECTOR_0;
	playerPosition = karbon_game::GetInstance().GetPlayerPosition();

	//Get real Direction to fire on outpost
	orxVECTOR directionToShip = orxVECTOR_0;
	orxVector_Sub(&directionToShip, &playerPosition, &enemyPosition);

	orxVECTOR speedTowardsShip = orxVECTOR_0;
	orxVector_Normalize(&speedTowardsShip, &directionToShip);
	orxVector_Mulf(&speedTowardsShip, &speedTowardsShip, 0.5);

//Only allow a capped speed
	orxObject_ApplyImpulse(enemyObject, &speedTowardsShip, orxNULL);

}


orxBOOL Enemy::OnCollide(ScrollObject *_poCollider,
	orxBODY_PART *_pstPart,
	orxBODY_PART *_pstColliderPart,
	const orxVECTOR &_rvPosition,
	const orxVECTOR &_rvNormal)
{
	if (_poCollider == orxNULL) {
		return orxTRUE;
	}

	const orxSTRING colliderName = _poCollider->GetModelName();
	if (orxString_SearchString(colliderName, "Ship") != orxNULL) {
		
		
		const orxSTRING name = orxBody_GetPartName(_pstColliderPart);
		orxU64 originalObjectID;
		orxLOG("part name %s", name);
		if (orxString_SearchString(name, "ShieldBodyPart") != orxNULL) {
			
			orxConfig_PushSection("PartTable");
			orxCHAR formattedPartPointerGet[32];
			orxString_NPrint(formattedPartPointerGet, sizeof(formattedPartPointerGet) - 1, "%016llX", _pstColliderPart);
			originalObjectID = orxConfig_GetU64(formattedPartPointerGet);
			
			orxConfig_PopSection();
			
			orxOBJECT *parent = orxOBJECT(orxStructure_Get(originalObjectID));
			if (parent != orxNULL){
				orxObject_SetLifeTime(parent, 0);
				orxLOG("part remove: %s", formattedPartPointerGet);
				orxBody_RemovePart(_pstColliderPart);
			}
			
		}
		
		//destroy if only bodypart remains, and no shield parts
		if (orxString_SearchString(name, "ShipBodyPart") != orxNULL) {
			int shieldsCount = ((Ship*)_poCollider)->CountShieldBodyParts();
			orxLOG("Shields count: %d", shieldsCount);
			
			if (shieldsCount == 0){
				orxOBJECT *expl = orxObject_CreateFromConfig("ExplosionMaker");
				
				orxVECTOR shipPosition = {};
				_poCollider->GetPosition(shipPosition, orxFALSE);
				orxObject_SetPosition(expl, &shipPosition);
				
				karbon_game::GetInstance().SetGameMode(GAME_MODE_TITLE);
			}
			
			
		}

		
		int here = 1;

	}

	return orxTRUE;
}


