#ifndef LASER_HPP
#define LASER_HPP

#include "karbon-game.h"

/** Object Class
 */
class Laser : public ScrollObject
{
public:


protected:

                void            OnCreate();
                void            OnDelete();
                void            Update(const orxCLOCK_INFO &_rstInfo);

private:
				
};

#endif // LASER_HPP
