##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Release_x64
ProjectName            :=karbon-particles
ConfigurationName      :=Release_x64
WorkspacePath          :=/Users/canberra-air/Documents/karbon-particles/build/mac/codelite
ProjectPath            :=/Users/canberra-air/Documents/karbon-particles/build/mac/codelite
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=canberra-air
Date                   :=11/07/2022
CodeLitePath           :="/Users/canberra-air/Library/Application Support/CodeLite"
LinkerName             :=/usr/bin/g++
SharedObjectLinkerName :=/usr/bin/g++ -dynamiclib -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../../../bin/karbon-particles
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="karbon-particles.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  -m64 -L/usr/lib64 -stdlib=libc++ -dead_strip -framework Foundation -framework AppKit
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)$(ORX)/include $(IncludeSwitch)../../../include/Scroll $(IncludeSwitch)../../../include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)orx 
ArLibs                 :=  "orx" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)$(ORX)/lib/dynamic $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++
CC       := /usr/bin/gcc
CXXFLAGS :=  -g -O2 -std=c++11 -ffast-math -m64 -fno-exceptions -fno-rtti -stdlib=libc++ -gdwarf-2 -Wno-unused-function -Wno-write-strings $(Preprocessors)
CFLAGS   :=  -g -O2 -std=c++11 -ffast-math -m64 -stdlib=libc++ -gdwarf-2 -Wno-unused-function -Wno-write-strings $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/Applications/codelite.app/Contents/SharedSupport/
ORX:=/Users/canberra-air/Documents/orx/code/
Objects0=$(IntermediateDirectory)/up_up_up_src_ProgressBar.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Shield.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Ship.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Laser.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Enemy.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_karbon-game.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

PostBuild:
	@echo Executing Post Build commands ...
	cp -f /Users/canberra-air/Documents/orx/code//lib/dynamic/liborx*.dylib ../../../bin
	@echo Done

MakeIntermediateDirs:
	@test -d $(ConfigurationName) || $(MakeDirCommand) $(ConfigurationName)


$(IntermediateDirectory)/.d:
	@test -d $(ConfigurationName) || $(MakeDirCommand) $(ConfigurationName)

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/up_up_up_src_ProgressBar.cpp$(ObjectSuffix): ../../../src/ProgressBar.cpp $(IntermediateDirectory)/up_up_up_src_ProgressBar.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/canberra-air/Documents/karbon-particles/src/ProgressBar.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_ProgressBar.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_ProgressBar.cpp$(DependSuffix): ../../../src/ProgressBar.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_ProgressBar.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_ProgressBar.cpp$(DependSuffix) -MM ../../../src/ProgressBar.cpp

$(IntermediateDirectory)/up_up_up_src_ProgressBar.cpp$(PreprocessSuffix): ../../../src/ProgressBar.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_ProgressBar.cpp$(PreprocessSuffix) ../../../src/ProgressBar.cpp

$(IntermediateDirectory)/up_up_up_src_Shield.cpp$(ObjectSuffix): ../../../src/Shield.cpp $(IntermediateDirectory)/up_up_up_src_Shield.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/canberra-air/Documents/karbon-particles/src/Shield.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Shield.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Shield.cpp$(DependSuffix): ../../../src/Shield.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Shield.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Shield.cpp$(DependSuffix) -MM ../../../src/Shield.cpp

$(IntermediateDirectory)/up_up_up_src_Shield.cpp$(PreprocessSuffix): ../../../src/Shield.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Shield.cpp$(PreprocessSuffix) ../../../src/Shield.cpp

$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(ObjectSuffix): ../../../src/Ship.cpp $(IntermediateDirectory)/up_up_up_src_Ship.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/canberra-air/Documents/karbon-particles/src/Ship.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(DependSuffix): ../../../src/Ship.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(DependSuffix) -MM ../../../src/Ship.cpp

$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(PreprocessSuffix): ../../../src/Ship.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Ship.cpp$(PreprocessSuffix) ../../../src/Ship.cpp

$(IntermediateDirectory)/up_up_up_src_Laser.cpp$(ObjectSuffix): ../../../src/Laser.cpp $(IntermediateDirectory)/up_up_up_src_Laser.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/canberra-air/Documents/karbon-particles/src/Laser.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Laser.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Laser.cpp$(DependSuffix): ../../../src/Laser.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Laser.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Laser.cpp$(DependSuffix) -MM ../../../src/Laser.cpp

$(IntermediateDirectory)/up_up_up_src_Laser.cpp$(PreprocessSuffix): ../../../src/Laser.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Laser.cpp$(PreprocessSuffix) ../../../src/Laser.cpp

$(IntermediateDirectory)/up_up_up_src_Enemy.cpp$(ObjectSuffix): ../../../src/Enemy.cpp $(IntermediateDirectory)/up_up_up_src_Enemy.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/canberra-air/Documents/karbon-particles/src/Enemy.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Enemy.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Enemy.cpp$(DependSuffix): ../../../src/Enemy.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Enemy.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Enemy.cpp$(DependSuffix) -MM ../../../src/Enemy.cpp

$(IntermediateDirectory)/up_up_up_src_Enemy.cpp$(PreprocessSuffix): ../../../src/Enemy.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Enemy.cpp$(PreprocessSuffix) ../../../src/Enemy.cpp

$(IntermediateDirectory)/up_up_up_src_karbon-game.cpp$(ObjectSuffix): ../../../src/karbon-game.cpp $(IntermediateDirectory)/up_up_up_src_karbon-game.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/Users/canberra-air/Documents/karbon-particles/src/karbon-game.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_karbon-game.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_karbon-game.cpp$(DependSuffix): ../../../src/karbon-game.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_karbon-game.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_karbon-game.cpp$(DependSuffix) -MM ../../../src/karbon-game.cpp

$(IntermediateDirectory)/up_up_up_src_karbon-game.cpp$(PreprocessSuffix): ../../../src/karbon-game.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_karbon-game.cpp$(PreprocessSuffix) ../../../src/karbon-game.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/


