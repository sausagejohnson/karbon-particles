##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug_x64
ProjectName            :=karbon-game
ConfigurationName      :=Debug_x64
WorkspacePath          :=C:/Work/Dev/orx-projects/karbon-game/build/windows/codelite
ProjectPath            :=C:/Work/Dev/orx-projects/karbon-game/build/windows/codelite
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=sausage
Date                   :=16/07/2022
CodeLitePath           :="C:/Program Files/CodeLite"
LinkerName             :=C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/g++.exe
SharedObjectLinkerName :=C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../../../bin/karbon-gamed.exe
Preprocessors          :=$(PreprocessorSwitch)__orxDEBUG__ 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="karbon-game.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/windres.exe
LinkOptions            :=  -static-libgcc -static-libstdc++ -m64 -L/usr/lib64
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)$(ORX)/include $(IncludeSwitch)../../../include/Scroll $(IncludeSwitch)../../../include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)orxd 
ArLibs                 :=  "orxd" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)$(ORX)/lib/dynamic $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/ar.exe rcu
CXX      := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/g++.exe
CC       := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/gcc.exe
CXXFLAGS :=  -ffast-math -g -m64 -fno-exceptions $(Preprocessors)
CFLAGS   :=  -ffast-math -g -m64 $(Preprocessors)
ASFLAGS  := 
AS       := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files (x86)\CodeLite
CC:=x86_64-w64-mingw32-gcc
CXX:=x86_64-w64-mingw32-g++
AR:=x86_64-w64-mingw32-gcc-ar
Objects0=$(IntermediateDirectory)/up_up_up_src_karbon-game.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Shield.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Laser.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Enemy.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_ProgressBar.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Ship.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

PostBuild:
	@echo Executing Post Build commands ...
	cmd /c copy /Y C:\Work\Dev\orx\code\lib\dynamic\orx*.dll ..\..\..\bin
	@echo Done

MakeIntermediateDirs:
	@$(MakeDirCommand) "$(ConfigurationName)"


$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "$(ConfigurationName)"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/up_up_up_src_karbon-game.cpp$(ObjectSuffix): ../../../src/karbon-game.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_karbon-game.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_karbon-game.cpp$(DependSuffix) -MM ../../../src/karbon-game.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/karbon-game/src/karbon-game.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_karbon-game.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_karbon-game.cpp$(PreprocessSuffix): ../../../src/karbon-game.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_karbon-game.cpp$(PreprocessSuffix) ../../../src/karbon-game.cpp

$(IntermediateDirectory)/up_up_up_src_Shield.cpp$(ObjectSuffix): ../../../src/Shield.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Shield.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Shield.cpp$(DependSuffix) -MM ../../../src/Shield.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/karbon-game/src/Shield.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Shield.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Shield.cpp$(PreprocessSuffix): ../../../src/Shield.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Shield.cpp$(PreprocessSuffix) ../../../src/Shield.cpp

$(IntermediateDirectory)/up_up_up_src_Laser.cpp$(ObjectSuffix): ../../../src/Laser.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Laser.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Laser.cpp$(DependSuffix) -MM ../../../src/Laser.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/karbon-game/src/Laser.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Laser.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Laser.cpp$(PreprocessSuffix): ../../../src/Laser.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Laser.cpp$(PreprocessSuffix) ../../../src/Laser.cpp

$(IntermediateDirectory)/up_up_up_src_Enemy.cpp$(ObjectSuffix): ../../../src/Enemy.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Enemy.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Enemy.cpp$(DependSuffix) -MM ../../../src/Enemy.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/karbon-game/src/Enemy.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Enemy.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Enemy.cpp$(PreprocessSuffix): ../../../src/Enemy.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Enemy.cpp$(PreprocessSuffix) ../../../src/Enemy.cpp

$(IntermediateDirectory)/up_up_up_src_ProgressBar.cpp$(ObjectSuffix): ../../../src/ProgressBar.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_ProgressBar.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_ProgressBar.cpp$(DependSuffix) -MM ../../../src/ProgressBar.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/karbon-game/src/ProgressBar.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_ProgressBar.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_ProgressBar.cpp$(PreprocessSuffix): ../../../src/ProgressBar.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_ProgressBar.cpp$(PreprocessSuffix) ../../../src/ProgressBar.cpp

$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(ObjectSuffix): ../../../src/Ship.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(DependSuffix) -MM ../../../src/Ship.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/karbon-game/src/Ship.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Ship.cpp$(PreprocessSuffix): ../../../src/Ship.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Ship.cpp$(PreprocessSuffix) ../../../src/Ship.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/


