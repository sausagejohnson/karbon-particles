/**
 * @file Object.h
 * @date 16-Jun-2022
 */

#ifndef __SHIELD_H__
#define __SHIELD_H__

#include "karbon-game.h"

/** Object Class
 */
class Shield : public ScrollObject
{
public:
				void		SetTrackToShip(orxBOOL);

protected:

                void            OnCreate();
                void            OnDelete();
                void            Update(const orxCLOCK_INFO &_rstInfo);
				orxBOOL 		OnCollide(ScrollObject *_poCollider,
									orxBODY_PART *_pstPart,
									orxBODY_PART *_pstColliderPart,
									const orxVECTOR &_rvPosition,
									const orxVECTOR &_rvNormal);
private:
				orxBOOL		trackToShip;
};

#endif // __SHIELD_H__
