/**
 * @file Object.h
 * @date 16-Jun-2022
 */

#ifndef __SHIP_H__
#define __SHIP_H__

#include "karbon-game.h"

/** Object Class
 */
class Ship : public ScrollObject
{
public:
	virtual void			ClearShieldBodyParts();
	int CountShieldBodyParts();
protected:

                void            OnCreate();
                void            OnDelete();
                void            Update(const orxCLOCK_INFO &_rstInfo);
				virtual orxBOOL OnCollide(ScrollObject *_poCollider,
					orxBODY_PART *_pstPart,
					orxBODY_PART *_pstColliderPart,
					const orxVECTOR &_rvPosition,
					const orxVECTOR &_rvNormal);
				virtual void			Thrust();
				virtual void			RotateLeft();
				virtual void			RotateRight();
				
				//void 			PostDebugSpot(orxVECTOR position);

private:
	ScrollObject* GetChildByName(const orxSTRING childName);
	ScrollObject* trigger;
};

#endif // __SHIP_H__
