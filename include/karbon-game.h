/**
 * @file karbon-game.h
 * @date 16-Jun-2022
 */

#ifndef __karbon_game_H__
#define __karbon_game_H__

#define __NO_SCROLLED__
#include "Scroll.h"
#include "Ship.h"

enum MODES { 
	GAME_MODE_TITLE,
	GAME_MODE_GATHER,
	GAME_MODE_DEFEND,
	GAME_MODE_GAMEOVER,
	GAME_MODE_NONE
};
//#define GAME_MODE_TITLE	0
//#define GAME_MODE_GATHER 1
//#define GAME_MODE_DEFEND 2
//#define GAME_MODE_GAMEOVER 3


/** Game Class
 */
class karbon_game : public Scroll<karbon_game>
{
public:
				ScrollObject *shield;
				
				orxOBJECT *shieldMaker;
				orxOBJECT *enemyMaker;

				
				orxVECTOR		GetPlayerPosition();
				MODES			GetGameMode();
				void			SetGameMode(MODES mode);

private:
				MODES gameMode = GAME_MODE_TITLE;
				MODES previousGameMode = GAME_MODE_NONE;

                orxSTATUS       Bootstrap() const;

                void            Update(const orxCLOCK_INFO &_rstInfo);

                orxSTATUS       Init();
                orxSTATUS       Run();
                void            Exit();
                void            BindObjects();
				void			RemoveAllFloatingShields();

};

#endif // __karbon_game_H__
