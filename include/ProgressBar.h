/**
 * @file Object.h
 * @date 6-Jul-2022
 */

#ifndef __PROGRESSBAR_H__
#define __PROGRESSBAR_H__

#include "karbon-game.h"

/** Object Class
 */
class ProgressBar : public ScrollObject
{
public:
    orxFLOAT        GetProgressPercent();
    void            SetProgressPercent(orxFLOAT percent);
    void            Pause(orxBOOL onOff);
    void            PanicMode();
    void            CalmMode();
protected:

    void            OnCreate();
    void            OnDelete();
    void            Update(const orxCLOCK_INFO &_rstInfo);
    ScrollObject*   GetChildByName(const orxSTRING childName);

private:
    orxFLOAT        progressPercent;
    orxBOOL         isPaused;
    void            ColourTheme(orxVECTOR backgroundColour, orxVECTOR levelColour);
};

#endif // __PROGRESSBAR_H__
