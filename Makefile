.PHONY: clean All

All:
	@echo "----------Building project:[ karbon-game - Debug_x64 ]----------"
	@"$(MAKE)" -f  "karbon-game.mk" && "$(MAKE)" -f  "karbon-game.mk" PostBuild
clean:
	@echo "----------Cleaning project:[ karbon-game - Debug_x64 ]----------"
	@"$(MAKE)" -f  "karbon-game.mk" clean
